import socket
import threading
import re
from model import Model


class Server(threading.Thread):

    def setup(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.connect(('google.com', 0))
        ipadress = self.sock.getsockname()[0]
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((ipadress, Model.UDP_PORT))
        print "Server Initialized"
        print ipadress

    def run(self):
        self.setup()
        while True:
            data, addr = self.sock.recvfrom(1024)  # buffer size is 1024 bytes
            print "received message:", data
            mx = re.search('(?<=X:)\d+', data)
            my = re.search('(?<=Y:)\d+', data)
            if mx is not None:
                if my is not None:
                    Model.template_X = int(mx.group(0))
                    Model.template_Y = int(my.group(0))
                    Model.startMatchingfromUDP = True
