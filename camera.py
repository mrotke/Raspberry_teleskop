import cv2 as cv
import shutil
from Tkinter import *
from PIL import Image
from PIL import ImageTk
import imutils


class camera:

    def __init__(self, x=0):
        self.capture = cv.VideoCapture(x)
        self.capture.set(cv.CAP_PROP_FPS, 1)
        self.capture.set(cv.CAP_PROP_FRAME_WIDTH, 640)
        self.capture.set(cv.CAP_PROP_FRAME_HEIGHT, 480)
        self.rawImage = None

    def grabFrame(self):
        ret, self.rawImage = self.capture.read()
        return ret, self.rawImage

    def convert2ImageTK(self, img):
        # OpenCV represents images in BGR order; however PIL represents
        # images in RGB order, so we need to swap the channels
        img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        # convert the images to PIL format...
        img = Image.fromarray(img)
        # ...and then to ImageTk format
        img = ImageTk.PhotoImage(img)
        return img

    def display(self):
        # grab grabFrame
        ret, img = self.grabFrame()
        if (ret):
            return self.convert2ImageTK(img), img

    def stop(self):
        self.capture.release()
