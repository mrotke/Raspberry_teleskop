
from Tkinter import *
from PIL import Image
from PIL import ImageTk
from camera import camera
import os
import numpy as np
import cv2 as cv
from model import Model
import UDPServer
#


class App:
    def __init__(self, window, window_title, video_source=0):
        self.window = window
        self.window.title(window_title)
        self.cam = camera()
        self.panel = None
        self.capturingFlag = False
        self.startMatching = False
        self.id = 0
        self.btn = Button(root, text="Start", command=self.clickBtn)
        self.btn.pack(
            side="bottom",
            fill="both",
            expand="yes",
            padx="10",
            pady="10")
        blank_image = np.zeros((50, 50, 3), np.uint8)
        blank_image = self.cam.convert2ImageTK(blank_image)
        self.panelROI = Label(image=blank_image)
        self.panelROI.pack(side="right", padx="10", pady="10")
        self.delay = 100
        self.rawImage = None
        self.templateImage = None
        self.server = UDPServer.Server()
        self.server.start()
#        self.update()
        self.window.mainloop()

    def update(self):
        imgTk, img = self.cam.display()
        if self.panel is None:
            # the first panel will store our original image
            self.panel = Label(image=imgTk)
            self.panel.bind("<Button 1>", self.imgClicked)
            self.panel.pack(side="left", padx=10, pady=10)
    # otherwise, update the image panels
        else:
            # update the pannels
            if Model.startMatchingfromUDP is True:
                self.SetRoi(Model.template_Y, Model.template_X)
                Model.startMatchingfromUDP = False
            elif self.startMatching is True:
                self.templateMatch()
            else:
                self.panel.configure(image=imgTk)
                self.panel.image = imgTk
        timg = cv.resize(img, (320, 240))
        timg = cv.rotate(timg, rotateCode=0)
        cv.imwrite('/home/michal/serwer/pic_tmp.png', timg)
        os.rename(
            '/home/michal/serwer/pic_tmp.png',
            '/home/michal/serwer/pic.png')
        self.window.after_cancel(self.id)
        self.id = self.window.after(self.delay, self.update)

    def clickBtn(self):
        if self.capturingFlag is False:
            self.capturingFlag = True
            self.btn.configure(text="Stop")
            self.id = self.window.after(self.delay, self.update)
        else:
            self.capturingFlag = False
            self.btn.configure(text="Start")
            self.window.after_cancel(self.id)

    def imgClicked(self, event):
        print "clicked at", event.x, event.y
        self.SetRoi(event.x, event.y)

    def templateMatch(self):
        # Apply template Matching
        res = cv.matchTemplate(self.cam.rawImage, self.templateImage, cv.TM_SQDIFF)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        top_left = min_loc
        print "match founda at", top_left[0], top_left[1]
        bottom_right = (top_left[0] + 50, top_left[1] + 50)
        cv.rectangle(self.cam.rawImage, top_left, bottom_right, 255, 2)
        img = self.cam.convert2ImageTK(self.cam.rawImage)
        self.panel.configure(image=img)
        self.panel.image = img

    def SetRoi(self, x, y):
        self.cam.grabFrame()
        self.templateImage = self.cam.rawImage[y -
                                               25 if y -
                                               25 > 0 else 0:y +
                                               25, x -
                                               25 if x -
                                               25 > 0 else 0:x +
                                               25]
        img = self.cam.convert2ImageTK(self.templateImage)
        self.panelROI.configure(image=img)
        self.panelROI.image = img
        self.templateMatch()
        self.startMatching = True


# initialize the window toolkit along with the two image panels
root = Tk()
App(root, "Tkinter and OpenCV")
